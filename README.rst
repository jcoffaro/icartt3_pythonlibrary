========
Overview
========

icartt3 takes work done on https://pypi.org/project/icartt/ and update the packages to be used with python 3 and fix a
bug regarding sensor times less than 1 second

* Free software: GNU Lesser General Public License v3 or later (LGPLv3+)

Installation
============

::

    pip install icartt3

You can also install the in-development version with::

    pip install git+ssh://git@https://gitlab.com/jcoffaro/icartt3_python_library.git/jcoffaro/python-icartt3.git@master

Documentation
=============


https://python-icartt3.readthedocs.io/


Development
===========

To run the all tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
